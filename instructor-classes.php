<?php
// Initialize the session
session_start();
require_once "config.php";

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}

$instructor_id = " ";
$email = $_SESSION["email"];
$sql = "SELECT Employee_id FROM `employee` WHERE employee.Email = '$email' ";
$result = mysqli_query($link, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
  
      // set variables for use in HTML
      $instructor_id = $row["Employee_id"];
    }
  
  } else {
  }

//====================================================================================================
?>
 
 <!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Bookings</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <style>
    body {
      font: 14px sans-serif;
      text-align: center;
    }
  </style>

</head>

<body>
  <h2>Your Upcoming Classes</h2>
  <br>
  <br>
    
  <div class="container">
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Employee Id</th>
          <th scope="col">Class</th>
          <th scope="col">Start Time</th>
          <th scope="col">End Time</th>
          <th scope="col">Date</th>
          <th scope="col">Room number</th>
    
        </tr>
      </thead>
      <tbody>
        <?php


        $sql = "SELECT Employee_id, Class_no, Date, Start_time, End_time, Room_no FROM `class_and_instructors`
        NATURAL JOIN `class`
        WHERE `class_and_instructors`.Employee_id = '$instructor_id'
        ORDER BY Date";
        $result = mysqli_query($link, $sql);
        if ($result) {

            $roomNum = " ";
          while ($row = mysqli_fetch_assoc($result)) {
            // Calculate name and type
            $emp_id = $row['Employee_id'];
            $classNum = $row['Class_no'];
            $date = $row['Date'];
            $endtime = $row['End_time'];
            $starttime = $row['Start_time'];
            $roomNum = $row['Room_no'];

            if(is_null($row['Room_no'])) {
                $roomNum = "No Room";
  
            }
            echo ' <tr>
            <th scope="row">' . $emp_id . '</th>
            <td>' . $classNum . '</td>
            <td>' . $starttime . '</td>
            <td>' . $endtime . '</td>
            <td>' . $date . '</td>
            <td>' . $roomNum . '</td>
            </td>
            </tr>';
          }
        }

        ?>

      </tbody>
    </table>
  </div>

  <a href="instructor-page.php" class="btn btn-secondary">Back</a>

</body>

</html>