<?php
// Initialize the session
session_start();
require_once "config.php";

 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}

$name = $address = "";
$age = 0;
$name_err = $address_err = $age_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Check if name is empty
        if(empty(trim($_POST["name"]))){
            $name_err = "Please enter your name.";
        } else{
            $name = trim($_POST["name"]);
        }

        // Check if address is empty
        if(empty(trim($_POST["address"]))){
            $address_err = "Please enter your address.";
        } else{
            $address = trim($_POST["address"]);
        }

        // Check if age is empty
        if(empty(trim($_POST["age"]))){
            $age_err = "Please enter your age.";
        } else{
            $age = trim($_POST["age"]);
        }
        echo "<script>console.log('pass' );</script>";

        // validate our inputs before entering into db
        if(empty($name_err) && empty($address_err) && empty($age_err)){
            $email = $_SESSION["email"];
            echo "<script>console.log('testing' );</script>";
            

            $sql = "UPDATE login SET Name= '$name', Address='$address', Age=$age WHERE Email='$email' ";

            if (mysqli_query($link, $sql)) {

                header("location: welcome.php");
              } else {
                echo "Error updating record: " . mysqli_error($link);
                echo "<script>console.log('fail' );</script>";

              }
              
              mysqli_close($link);

        }




}

?>


 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Setup</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
<div class="container">
        <h2>Settings</h2>
        <p>Please fill in your information to get started!</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control <?php echo (!empty($name_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $name; ?>">
                <span class="invalid-feedback"><?php echo $name_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Address</label>
                <input type="text" name="address" class="form-control <?php echo (!empty($address_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $address; ?>">
                <span class="invalid-feedback"><?php echo $address_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Age</label>
                <input type="number" name="age" class="form-control <?php echo (!empty($age_err)) ? 'is-invalid' : ''; ?>?>">
                <span class="invalid-feedback"><?php echo $age_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-secondary ml-2" value="Reset">
            </div>
        </form>
    </div>    
</body>
</html>