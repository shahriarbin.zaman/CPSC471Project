<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gym Management</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <?php 
                    if(isset($_SESSION['status']))
                    {
                        ?>
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Hey!</strong> <?php echo $_SESSION['status']; ?>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        <?php
                        unset($_SESSION['status']);
                    }
                ?>

                <div class="card mt-5">
                    <div class="card-header">
                        <h4>Data will be updated based on the email. Please enter the email and update corressponding values</h4>
                    </div>
                    <div class="card-body">

                        <form action="code_to_update_emp.php" method="POST">

                            <div class="form-group mb-3">
                                <label for="">Email.</label>
                                <input type="email" name="Email" class="form-control" >
                            </div>
                            <div class="form-group mb-3">
                                <label for="">Name</label>
                                <input type="text" name="Name" class="form-control" >
                            </div>
                            <div class="form-group mb-3">
                                <label for="">Address</label>
                                <input type="text" name="Address" class="form-control" >
                            </div>
                            <div class="form-group mb-3">
                                <label for="">Age</label>
                                <input type="number" name="Age" class="form-control" >
                            </div>
                            <div class="form-group mb-3">
                                <label for="">Position</label>
                                <input type="text" name="Position" class="form-control" >
                            </div>
                            <!-- <div class="form-group mb-3">
                                <label for="">Login_Type</label>
                                <input type="number" name="Login_Type" class="form-control" >
                            </div> -->
                           

                            <!-- <div class="form-group mb-3">
                                <label for="">Membership_is_active</label>
                                <input type="number" name="Membership_is_active" class="form-control" >
                            </div> -->
                            <!-- <div class="form-group mb-3">
                                <label for="">Member_id</label>
                                <input type="number" name="Member_id" class="form-control" >
                            </div> -->
                            <div class="form-group mb-3">
                                <button type="submit" name="update_emp_data" class="btn btn-primary">Update Data</button>
                                <a href="welcome_manager.php" class="btn btn btn-secondary">Back</a>

                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>