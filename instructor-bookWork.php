<?php
// Initialize the session
session_start();
require_once "config.php";

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}


//get instructor id ========================================================
$instructor_id = " ";
$email = $_SESSION["email"];
$sql = "SELECT Employee_id FROM `employee` WHERE employee.Email = '$email' ";
$result = mysqli_query($link, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
  
      // set variables for use in HTML
      $instructor_id = $row["Employee_id"];
    }
  
  } else {
    echo "0 results";
  }

//==========================================================================

$member_id = $activity =  "";
$member_id_err =$activity_err = "";
$bookingDate = $bookingDuration = "";
$booking_err = "";
if($_SERVER["REQUEST_METHOD"] == "POST") {

    // Check if date is empty
    if(empty(trim($_POST["bookingDate"]))) {
        $booking_err = "Could not book";
    } else {
        $bookingDate = $_POST["bookingDate"];
    }


    if(empty(trim($_POST["bookingDuration"]))) {
        $booking_err = "Could not book";
    } else {
        $bookingDuration = $_POST["bookingDuration"];
    }

    if(empty(trim($_POST["activity"]))) {
        $activity_err = "Activity task is needed";
    } else {
        $activity = $_POST["activity"];
    }

    // Validate member id
    if(empty(trim($_POST["memberId"]))){
        $member_id_err = "Please enter a member's id.";
    } 
    
    else{
        // Prepare a select statement
        $sql = "SELECT Member_id FROM `members` WHERE Member_id = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $paramid);
            
            // Set parameters
            $paramid = trim($_POST["memberId"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 0){
                    $member_id_err = "Not a valid member id";
                } else{
                    $member_id = trim($_POST["memberId"]);

                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }


    // validate before entering into db
    if(empty($booking_err) && empty($activity_err) && empty($member_id_err)){

        echo "<script>console.log('{$bookingDate}')</script>";

        $sql = "INSERT INTO `workout_plan` (Duration, Date) VALUES (?, ?)";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "is", $param_duration, $param_date);
            // Set parameters
            if($bookingDuration == "+ 30 minutes") {
                $param_duration = 30;
            } else if($bookingDuration == "+ 60 minutes") {
                $param_duration = 60;
            } else if($bookingDuration == "+ 90 minutes") {
                $param_duration = 90;
            } else if($bookingDuration == "+ 120 minutes") {
                $param_duration = 120;
            }
            $param_date = date($bookingDate);

            // Attempt to execute the prepared statement

            if(mysqli_stmt_execute($stmt)){
                $lastid = mysqli_insert_id($link);
            echo "<script>console.log('success' );</script>";

            } else{
                echo "<script>console.log('error' );</script>";
                echo mysqli_error($link);
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }

        $sql2 = "INSERT INTO `workout_plan_activity` (`Plan#`, Activity_name) VALUES (?, ?)";
        if($stmt2 = mysqli_prepare($link, $sql2)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt2, "is", $param_plan_id, $param_activity);

            $param_plan_id = $lastid;
            $param_activity = $activity;

            
            // Attempt to execute the prepared statement

            if(mysqli_stmt_execute($stmt2)){
                // Redirect to login page
            echo "<script>console.log('success' );</script>";
            } else{
                echo "<script>console.log('error' );</script>";
                echo mysqli_error($link);
            }

            // Close statement
            mysqli_stmt_close($stmt2);
        }

        $sql3 = "INSERT INTO `plan_created_for` (`Plan#`, Member_id, Instructor_employee_id) VALUES (?, ?, ?)";
        if($stmt3 = mysqli_prepare($link, $sql3)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt3, "iii", $param_plan_id, $param_member_id, $param_instr_id);

            $param_plan_id = $lastid;
            $param_member_id = $member_id;
            $param_instr_id = $instructor_id;

            echo "MEMBER ID:: $member_id";
            // Attempt to execute the prepared statement

            if(mysqli_stmt_execute($stmt3)){
                // Redirect to login page
            echo "<script>console.log('success' );</script>";
            header("location: instructor-bookWork.php");

            } else{
                echo "<script>console.log('error' );</script>";
                echo mysqli_error($link);
            }

            // Close statement
            mysqli_stmt_close($stmt3);
        }
          

    }

    

    
}


mysqli_close($link);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="lib/bootstrap-datepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap-datepicker.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script>
        $(function() {
            $('.dates #usr1').datepicker({
                'format': 'yyyy-mm-dd',
                'autoclose': true
            });
        });
    </script>
    <style>
        body {
            font: 14px sans-serif;
            text-align: center;
        }
        .wrapper{ width: 360px; padding: 20px; }
    </style>
</head>


<body>
    <h2>Create A Workout Plan</h2>

    <hr>
    <div class="container">

        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="usr1">Date</label>
                    <div class="dates">
                        <input type="text" class="form-control" id="usr1" name="bookingDate" placeholder="YYYY-MM-DD" autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                <label>Member ID</label>
                <input type="number" name="memberId" class="form-control <?php echo (!empty($member_id_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $member_id; ?>">
                <span class="invalid-feedback"><?php echo $member_id_err; ?></span>
                </div> 
                <div class="form-group">
                <label>Activity</label>
                <input type="text" name="activity" class="form-control <?php echo (!empty($activity_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $activity; ?>">
                <span class="invalid-feedback"><?php echo $activity_err; ?></span>
                </div>  
                
                <div class="form-group col-md-6">
                    <label for="inputDuration">Duration (minutes)</label>
                    <select id="inputDuration" class="form-control" name="bookingDuration">
                        <option selected>Choose...</option>
                        <option value="+ 30 minutes">30 Minutes</option>
                        <option value="+ 60 minutes">60 Minutes (1 Hour)</option>
                        <option value="+ 90 minutes">90 Minutes</option>
                        <option value="+ 120 minutes">120 Minutes (2 Hours)</option>
                    </select>
                </div>
            </div>
            <input type="submit" class="btn btn-primary" value="Submit">


        </form>
    </div>
    <br>
    <a href="instructor-page.php" class="btn btn-secondary">Back</a>



    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


</body>

</html>