<?php
// Initialize the session
include 'config.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crud Operations</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
    </style>
    
</head>
<body>
    <div class="container">
        <button class = "my-5"><a href="signup_members_by_emp.php">Add Members</a>
            </button>
            <table class="table">
  <thead>
    <tr>
      <th scope="col">Member_id</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Address</th>
      <th scope="col">Age</th>
      
      <th scope="col">Member_start_date</th>
      <th scope="col">Member_expiry_date</th>
      <th scope="col">Active?</th>
      <th scope="col">Operations</th>

    </tr>
  </thead>
  <tbody>
  <?php

$status = False;
$sql = "SELECT * from `login` INNER JOIN `members` ON login.Email = members.Email";
$result = mysqli_query($link,$sql);

if($result){
    
    while($row= mysqli_fetch_assoc($result)){
        $Member_id = $row['Member_id'];
        $Name = $row['Name'];
        $Email= $row['Email'];
        $Address = $row['Address'];
        $Age = $row['Age'];
        
        $Member_start_date = $row["Member_start_date"];
        $Member_expiry_date = $row["Member_expiry_date"];
        if($row["Membership_is_active"] == 1) {
            $status = True;
        } else {
            $status = False;
        }
        echo ' <tr>
        <th scope="row">'.$Member_id.'</th>
        <td>'.$Name.'</td>
        <td>'.$Email.'</td>
        <td>'.$Address.'</td>
        <td>'.$Age.'</td>
        <td>'.$Member_start_date.'</td>
        <td>'.$Member_expiry_date.'</td>
        <td>'.$status.'</td>
        <td>
        <button><a href="delete_mem_by_emp.php?deleteMemberid='.$Member_id.'">Delete</a></button>
       </td>
 
      </tr>';



      }

}




  ?>
  



  
    
  </tbody>
</table>
<a href="welcome_emp.php" class="btn btn-secondary">Back</a>

    </div>

    
</body>
</html>