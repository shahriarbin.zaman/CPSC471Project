<?php
// Initialize the session
session_start();
require_once "config.php";

 
//Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;

}

echo "<script>console.log('test')</script>";

// Initialize variables

//$name = $address = $emp_id= $position= $mngr_emp_id = "";


$email = $_SESSION["email"];
echo "<script>console.log($email)</script>";
$sql = "SELECT Name, Address, Employee_id,Position,Mngr_emp_id FROM `employee`  INNER JOIN `login` ON employee.Email = login.Email WHERE login.Email = '$email' ";
echo "<script>console.log('test4')</script>";
$result = mysqli_query($link, $sql);
echo "<script>console.log('test5')</script>";
if (mysqli_num_rows($result) > 0) {
  // output data of each row
  echo "<script>console.log('test2')</script>";
  while($row = mysqli_fetch_assoc($result)) {

    echo "<script>console.log('test3')</script>";
    // set variables for use in HTML
    $name = $row["Name"];
    $address = $row["Address"];
    $emp_id = $row["Employee_id"];
    $position = $row["Position"];
    $mngr_emp_id = $row["Mngr_emp_id"];

    
  }

//   echo $name ." " . $start . " " .  $expiry . " " .  $status;


} else {
  echo "0 results";
}

mysqli_close($link);



?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <h1 class="my-5">Hi, <?php echo htmlspecialchars($name); ?>.</h1>
    <h2>Here is your employee information:</h2>

    <ul class = "list-group">
        <li class = "list-group-item">Employee Name: <?php echo htmlspecialchars($name); ?></li>
        <li class = "list-group-item">Address: <?php echo htmlspecialchars($address); ?></li>
        <li class = "list-group-item">Employee ID: <?php echo htmlspecialchars($emp_id); ?></li>
        <li class = "list-group-item">Position: <?php echo htmlspecialchars($position); ?></li>
        <li class = "list-group-item">Manager ID: <?php echo htmlspecialchars($mngr_emp_id); ?></li>
        
    </ul>
    <br>
    
    <br>
    <br>
    <a href="welcome_emp.php" class="btn btn-secondary">Back</a>
    
    


</body>
</html>