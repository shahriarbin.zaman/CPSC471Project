<?php
// Initialize the session
session_start();
require_once "config.php";

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}


//get instructor id ========================================================
$instructor_id = " ";
$email = $_SESSION["email"];
$sql = "SELECT Employee_id FROM `employee` WHERE employee.Email = '$email' ";
$result = mysqli_query($link, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
  
      // set variables for use in HTML
      $instructor_id = $row["Employee_id"];
    }
  
  } else {
    echo "0 results";
  }

//==========================================================================
$roomSQL = "SELECT Room_no, Facility_id FROM `room` ";
$roomResults = mysqli_query($link, $roomSQL);


$bookingDate = $bookingTime = $bookingRoomNumber = $bookingDuration = "";
if($_SERVER["REQUEST_METHOD"] == "POST") {

    // Check if date is empty
    if(empty(trim($_POST["bookingDate"]))) {
        $booking_err = "Could not book";
    } else {
        $bookingDate = trim($_POST["bookingDate"]);
    }


    // Check if time is empty

    if(empty(trim($_POST["inputTime"]))) {
        $booking_err = "Could not book";
    } else {
        $bookingTime = trim($_POST["inputTime"]);
    }

    if(empty(trim($_POST["roomNumber"]))) {
        $booking_err = "Could not book";
    } else if(trim($_POST["roomNumber"]) == "No Room"){
        $bookingRoomNumber = null;
    }else {
        $bookingRoomNumber = trim($_POST["roomNumber"]);
    }

    if(empty(trim($_POST["bookingDuration"]))) {
        $booking_err = "Could not book";
    } else {
        $bookingDuration = $_POST["bookingDuration"];
    }

    // validate before entering into db
    if(empty($booking_err)){
        
        echo "<script>console.log('{$bookingTime}' );</script>";
        echo "<script>console.log('{$bookingDuration}' );</script>";
        echo "<script>console.log('{$bookingDate}' );</script>";
        
        // Take the duration and add it to the start time to get the end time


        $endtime = date('H:i', strtotime($bookingDuration, strtotime($bookingTime)));


        $sql = "INSERT INTO `class` (Date, Start_time, End_time, Room_no) VALUES (?, ?, ?, ?)";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sssi", $param_date, $param_start, $param_end, $param_room);
            // Set parameters

            $param_date = date($bookingDate);
            $param_start = date($bookingTime);
            $param_end = $endtime;
            $param_room = $bookingRoomNumber;

            echo "ID ->> $param_date";

            
            // Attempt to execute the prepared statement

            if(mysqli_stmt_execute($stmt)){
                $lastid = mysqli_insert_id($link);
            echo "<script>console.log('success' );</script>";

            } else{
                echo "<script>console.log('error' );</script>";
                echo mysqli_error($link);
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }

        $sql2 = "INSERT INTO `class_and_instructors` (Employee_id, Class_no) VALUES (?, ?)";
        if($stmt2 = mysqli_prepare($link, $sql2)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt2, "ii", $param_emp_id, $param_class_no);

            $param_emp_id = $instructor_id;
            $param_class_no = $lastid;

            
            // Attempt to execute the prepared statement

            if(mysqli_stmt_execute($stmt2)){
                // Redirect to login page
            echo "<script>console.log('success' );</script>";
            header("location: instructor-bookClass.php");

            } else{
                echo "<script>console.log('error' );</script>";
                echo mysqli_error($link);
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }


          

    }

    

    
}


mysqli_close($link);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="lib/bootstrap-datepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap-datepicker.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script>
        $(function() {
            $('.dates #usr1').datepicker({
                'format': 'yyyy-mm-dd',
                'autoclose': true
            });
        });
    </script>
    <style>
        body {
            font: 14px sans-serif;
            text-align: center;
        }
    </style>
</head>


<body>
    <h2>Book A Class</h2>

    <hr>
    <div class="container">

        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="usr1">Date</label>
                    <div class="dates">
                        <input type="text" class="form-control" id="usr1" name="bookingDate" placeholder="YYYY-MM-DD" autocomplete="off">
                    </div>
                </div>
                <div class="form-group col-md-6">
                <label for="inputTime">Time of Booking</label>
                    <select id="inputTime" class="form-control" name="inputTime">
                        <option selected>Choose...</option>
                        <option value="09:00">09:00 AM</option>
                        <option value="10:00">10:00 AM</option>
                        <option value="11:00">11:00 AM</option>
                        <option value="12:00">12:00 PM</option>
                        <option value="13:00">01:00 PM</option>
                        <option value="14:00">02:00 PM</option>
                        <option value="15:00">03:00 PM</option>
                        <option value="16:00">04:00 PM</option>
                        <option value="17:00">05:00 PM</option>
                        <option value="18:00">06:00 PM</option>
                        <option value="19:00">07:00 PM</option>
                        <option value="20:00">08:00 PM</option>
                        <option value="21:00">09:00 PM</option>
                        <option value="22:00">10:00 PM</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputName">Room Number</label>
                    <select id="inputName" class="form-control" name="roomNumber">
                        <?php
                        // use a while loop to fetch data
                        // from the $all_categories variable
                        // and individually display as an option
                        while ($room = mysqli_fetch_array(
                            $roomResults,
                            MYSQLI_ASSOC
                        )) :;
                        ?>
                            <option value="<?php echo $room["Facility_id"];
                                            // The value we usually set is the primary key
                                            ?>">
                                Room <?php echo $room["Room_no"];
                                // To show the category name to the user
                                ?>
                            </option>
                        <?php
                        endwhile;
                        
                        // While loop must be terminated
                        ?>
                            <option value="No Room">
                                No Room
                            </option>                       
                    </select>
                </div>

                <div class="form-group col-md-6">
                    <label for="inputDuration">Duration (minutes)</label>
                    <select id="inputDuration" class="form-control" name="bookingDuration">
                        <option selected>Choose...</option>
                        <option value="+ 30 minutes">30 Minutes</option>
                        <option value="+ 60 minutes">60 Minutes (1 Hour)</option>
                        <option value="+ 90 minutes">90 Minutes</option>
                        <option value="+ 120 minutes">120 Minutes (2 Hours)</option>
                    </select>
                </div>
            </div>
            <input type="submit" class="btn btn-primary" value="Submit">


        </form>
    </div>
    <br>
    <a href="instructor-page.php" class="btn btn-secondary">Back</a>



    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


</body>

</html>