<?php
// Initialize the session
session_start();
include 'config.php';
$email = $_SESSION["email"];

$initialSQL = "SELECT Name, Member_id FROM `members` INNER JOIN `login` ON members.Email = login.Email WHERE login.Email = '$email' ";



// We want to first view the room bookings

$name = $member_id = "";

$result = mysqli_query($link, $initialSQL);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while ($row = mysqli_fetch_assoc($result)) {
        // set variables for use in HTML
        $name = $row["Name"];
        $member_id = $row["Member_id"];
    }
} else {
    echo "0 results";
}



?>



<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Classes</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <style>
    body {
      font: 14px sans-serif;
      text-align: center;
    }
  </style>

</head>

<body>
  <h2>Here is a list of all upcoming courses:</h2>
  <br>
  <br>
    
  <div class="container">
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Date</th>
          <th scope="col">Start Time</th>
          <th scope="col">End Time</th>
          <th scope="col">Room number</th>
          <th scope="col">Instructor</th>    
          <th scope="col">Specialization</th>    
        </tr>
      </thead>
      <tbody>
        <?php



        $sql = "SELECT Date, Start_time, End_time, Room_no, Name, Specialization, class.Class_no FROM `class`
        LEFT OUTER JOIN `class_and_instructors` on `class`.class_no = `class_and_instructors`.class_no
        LEFT OUTER JOIN `employee` on `employee`.Employee_id = `class_and_instructors`.Employee_id 
        LEFT OUTER JOIN `takes` on `takes`.`Class_no`= `class`.`Class_no`
        LEFT OUTER JOIN `login` on `login`.Email = `employee`.Email
        LEFT OUTER JOIN `instructor` on `instructor`.`Instructor_employee_id` = `employee`.`Employee_id`
        WHERE Member_id <> $member_id OR Member_id IS NULL";
        $result = mysqli_query($link, $sql);
        if ($result) {

          $facilityname = $bookingtype =  "";
          while ($row = mysqli_fetch_assoc($result)) {
            // Calculate name and type

            $date = $row['Date'];
            $starttime = $row['Start_time'];
            $endtime = $row['End_time'];
            $roomno = $row['Room_no'];
            $instructorName = $row['Name'];
            $specialization = $row['Specialization'];
            $classno = $row['Class_no'];


            echo ' <tr>
        <th scope="row">' . $date . '</th>
        <td>' . $starttime . '</td>
        <td>' . $endtime . '</td>
        <td>' . $roomno . '</td>
        <td>' . $instructorName . '</td>
        <td>' . $specialization . '</td>
        <td>
        <button><a href="MemberRegisterClass.php?memberToRegister='.$member_id.'&classRegisterNo='.$classno.'">Register</a></button>
        </td>
       </td>
 
      </tr>';
          }
        }

        ?>

      </tbody>
    </table>
  </div>

  <br>
  <hr>
  <a href="MemberClasses.php" class="btn btn-secondary">Back</a>

</body>

</html>