<?php
// Initialize the session
session_start();
require_once "config.php";

 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;

}

// Initialize variables

$name = $start = $expiry = $memberid = "";
$status = False;

$email = $_SESSION["email"];

$sql = "SELECT Member_id, Name, Member_start_date, Member_expiry_date, Membership_is_active FROM `members` INNER JOIN `login` ON members.Email = login.Email WHERE login.Email = '$email' ";

$result = mysqli_query($link, $sql);
  
if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while ($row = mysqli_fetch_assoc($result)) {
        // set variables for use in HTML
        
        $memberid = $row["Member_id"];
        $name = $row["Name"];
        $start = $row["Member_start_date"];
        $expiry = $row["Member_expiry_date"];

        if($row["Membership_is_active"] == 1) {
            $status = True;
        } else {
            $status = False;
        }
    }
} else {
    echo "0 results";
}

echo "<script>console.log('{$expiry}' );</script>";
echo "<script>console.log('{$name}' );</script>";
echo "<script>console.log('{$expiry}' );</script>";



if(array_key_exists('1week', $_POST)) {
    
    $expiry = date("Y-m-d", strtotime("+7 day", strtotime($expiry)));
    $updatesql = "UPDATE `members` SET Member_expiry_date = '$expiry' WHERE Member_id = $memberid";
    if (mysqli_query($link, $updatesql)) {
        echo "Record updated successfully";
      } else {
        echo "Error updating record: " . mysqli_error($link);
        echo "<script>console.log('{$expiry}' );</script>";
      }


} else if(array_key_exists('1month', $_POST)) {

    $expiry = date("Y-m-d", strtotime("+30 day", strtotime($expiry)));
    $updatesql = "UPDATE `members` SET Member_expiry_date = '$expiry' WHERE Member_id = $memberid";
    if (mysqli_query($link, $updatesql)) {
        echo "Record updated successfully";
      } else {
        echo "Error updating record: " . mysqli_error($link);
        echo "<script>console.log('{$expiry}' );</script>";
      }

} else if(array_key_exists('3month', $_POST)) {
    $expiry = date("Y-m-d", strtotime("+3 month", strtotime($expiry)));
    $updatesql = "UPDATE `members` SET Member_expiry_date = '$expiry' WHERE Member_id = $memberid";
    if (mysqli_query($link, $updatesql)) {
        echo "Record updated successfully";
      } else {
        echo "Error updating record: " . mysqli_error($link);
        echo "<script>console.log('{$expiry}' );</script>";
      }

    
} 














mysqli_close($link);



?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <h1 class="my-5">Hi, <?php echo htmlspecialchars($name); ?>.</h1>
    <h2>Here is your membership information:</h2>

    <ul class = "list-group">   
        <li class = "list-group-item">Membership Start Date: <?php echo htmlspecialchars($start); ?></li>
        <li class = "list-group-item">Membership Expiry Date: <?php echo htmlspecialchars($expiry); ?></li>
        <li class = "list-group-item"><?php echo ($status) ? 'Currently a member!' : 'Not a member'; ?></li>
    </ul>
    <br>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Update your membership
    </button>
    <br>

    <br>
    <a href="welcome.php" class="btn btn-secondary">Back</a>
    

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Your Membership!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form method="post">
                        <input type="submit" name="1week" class="btn btn-primary btn-block" value="Update 1 week    $10" />
                        <br>    
                        <input type="submit" name="1month" class="btn btn-primary btn-block" value="Update 1 month  $40" />
                        <br>
                        <input type="submit" name="3month" class="btn btn-primary btn-block" value="Update 3 month  $100" />
                    </form>


                    <br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>