<?php
// Initialize the session
session_start();
require_once "config.php";

 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}


$id = 0;
$email = $_SESSION["email"];
$sql = "SELECT Employee_id FROM `employee` WHERE employee.Email = '$email' ";
$result = mysqli_query($link, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each column
    while($row = mysqli_fetch_assoc($result)) {
  
      // set variables for use in HTML
      $id = $row["Employee_id"];
    }
  
  } else {
    echo "0 results";
  }
  
  echo "<script>console.log('   {$id}' );</script>";

  mysqli_close($link);

?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome EMPLOYEE</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <h1 class="my-5">Hi employee: , <b><?php echo htmlspecialchars($id); ?></b>. Welcome to our site.</h1>
    <p>
        <a href="reset-password.php" class="btn btn-warning">Reset Your Password</a>
        <a href="logout.php" class="btn btn-danger ml-3">Sign Out of Your Account</a>
    </p>
</body>
</html>