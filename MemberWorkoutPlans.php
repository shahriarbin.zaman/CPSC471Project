<?php
// Initialize the session
session_start();
require_once "config.php";

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}

$member_id = " ";
$email = $_SESSION["email"];
$sql = "SELECT Member_id FROM `members` WHERE members.Email = '$email' ";
$result = mysqli_query($link, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
  
      // set variables for use in HTML
      $member_id = $row["Member_id"];
    }
  
  } else {
    echo "0 results";
  }

//====================================================================================================
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Bookings</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <style>
    body {
      font: 14px sans-serif;
      text-align: center;
    }
  </style>

</head>

<body>
  <h2>Your Workout Plans</h2>
  <br>
  <br>
    
  <div class="container">
    <table class="table">
      <thead>
        <tr>
            <th scope="col">Member ID</th>
            <th scope="col">Instructor ID</th>
            <th scope="col">Instructor Name</th>
            <th scope="col">Plan number</th>
            <th scope="col">Duration (mins)</th>
            <th scope="col">Date</th>
            <th scope="col">Activity Name</th>
    
        </tr>
      </thead>
      <tbody>
        <?php


        $sql = "SELECT `Plan#`, Activity_name, Member_id, Instructor_employee_id, Duration, `Name`, `Date` FROM `plan_created_for`
        NATURAL JOIN `workout_plan_activity` JOIN `workout_plan` ON `workout_plan`.Plan_id = `Plan#`
        JOIN `employee` ON `plan_created_for`.Instructor_employee_id = `employee`.Employee_id
        JOIN `login` ON `login`.Email = `employee`.Email
        WHERE Member_id = '$member_id'
        ORDER BY `Plan#`";
        $result = mysqli_query($link, $sql);
        if ($result) {

          while ($row = mysqli_fetch_assoc($result)) {
            // Calculate name and type
            $Plan = $row['Plan#'];
            $Activity_name = $row['Activity_name'];
            $Member_id = $row['Member_id'];
            $Instructor_employee_id = $row['Instructor_employee_id'];
            $Instructor_employee_name = $row['Name'];
            $duration = $row['Duration'];
            $date = $row['Date'];
            echo ' <tr>
            <th scope="row">' . $Member_id . '</th>
            <td>' . $Instructor_employee_id . '</td>
            <td>' . $Instructor_employee_name . '</td>
            <td>' . $Plan . '</td>
            <td>' . $duration . '</td>
            <td>' . $date . '</td>
            <td>' . $Activity_name . '</td>
            </td>
            </tr>';
          }
        }

        ?>

      </tbody>
    </table>
  </div>

  <a href="welcome.php" class="btn btn-secondary">Back</a>

</body>

</html>