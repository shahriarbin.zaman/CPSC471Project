# SQL-Front 5.1  (Build 4.16)




# Host: localhost    Database: 471
# ------------------------------------------------------
# Server version 5.0.51b-community-nt-log

DROP DATABASE IF EXISTS `471Project`;
CREATE DATABASE `471Project`;
USE `471Project`;


DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
    `Username`    varchar(20) NOT NULL,
    `Email`       varchar(50) NOT NULL,
    `Password`    varchar(255) NOT NULL,
    `Login_Type`  int         NOT NULL,
    `Address`     varchar(60),
    `Age`         int,
    `Name`        varchar(30),
    PRIMARY KEY (`Email`),
    UNIQUE(`Username`)
);

DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
    `Email`     varchar(50) NOT NULL,
    `Member_id` int         NOT NULL AUTO_INCREMENT,
    `Member_start_date`     date    NOT NULL,
    `Member_expiry_date`    date    NOT NULL,
    `Membership_is_active`  boolean    NOT NULL,
    PRIMARY KEY(`Member_id`, `Email`),
    FOREIGN KEY(`Email`) REFERENCES `login`(`email`)
    ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`
(
    `Email`        varchar(50),
    `Employee_id`    int NOT NULL AUTO_INCREMENT,
    `Position`    varchar(10),
    `Mngr_emp_id`     int,
    PRIMARY KEY(`Employee_id`,`Email`),
    FOREIGN KEY (`Mngr_emp_id`) REFERENCES `employee`(`Employee_id`),
    FOREIGN KEY(`Email`) REFERENCES `login`(`Email`) ON DELETE CASCADE ON UPDATE CASCADE
);



DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking`
(
    `Member_id`    int            NOT NULL,
    `Booking_id`    int            NOT NULL UNIQUE AUTO_INCREMENT,
    `Duration`    int            NOT NULL,
    `Date`        date            NOT NULL,
    `Start_time`    time            NOT NULL,
    `End_time`    time            NOT NULL,
    PRIMARY KEY(`Member_id`, `Booking_id`),
    FOREIGN KEY(`Member_id`) REFERENCES `members`(`Member_id`)        
);

DROP TABLE IF EXISTS `instructor`;
CREATE TABLE `instructor`
(
    `Instructor_employee_id`    int    NOT NULL,
    `Specialization`        varchar(10),
    PRIMARY KEY(`Instructor_employee_id`),
    FOREIGN KEY(`Instructor_employee_id` ) REFERENCES `employee`(`Employee_id`) ON DELETE CASCADE            ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `gym_facility`;
CREATE TABLE `gym_facility`
(
    `Facility_id`    int    NOT NULL UNIQUE,
    `Maintenance_date`    date,
    PRIMARY KEY(`Facility_id`)
);

DROP TABLE IF EXISTS `room`;
CREATE TABLE `room`
(
    `Room_no`    int    NOT NULL,
    `Facility_id`     int    NOT NULL,
    PRIMARY KEY(`Room_no`, `Facility_id`),
    FOREIGN KEY(`Facility_id`  ) REFERENCES `gym_facility`(`Facility_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `class`;
CREATE TABLE `class`
(
`Class_no`    int    NOT NULL AUTO_INCREMENT,
`Date`        date    NOT NULL,
`End_time`    TIME    NOT NULL,
`Start_time`    TIME    NOT NULL,
`Room_no`    int,
PRIMARY KEY(`Class_no`),
FOREIGN KEY(`Room_no`  ) REFERENCES `room`(`Room_no`)
);


DROP TABLE IF EXISTS `machine`;
CREATE TABLE `machine`
(
    `Machine_no`    int    NOT NULL,
    `Machine_type`    varchar(30)    ,
    `Facility_id`    int    NOT NULL,
    PRIMARY KEY(`Machine_no`, `Facility_id`),
    FOREIGN KEY(`Facility_id`  ) REFERENCES `gym_facility`(`Facility_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `workout_plan`;
CREATE TABLE `workout_plan`
(
    `Plan_id`        int    NOT NULL UNIQUE AUTO_INCREMENT,
    `Duration`    int    NOT NULL,
    `Date`        date    NOT NULL,
    PRIMARY KEY(`Plan_id`)
);

DROP TABLE IF EXISTS `takes`;
CREATE TABLE `takes`
(
    `Member_id`    int    NOT NULL,
    `Class_no`    int    NOT NULL,
    PRIMARY KEY(`Member_id`, `Class_no`),
    FOREIGN KEY(`Member_id`  ) REFERENCES `members`(`Member_id`),
    FOREIGN KEY(`Class_no`  ) REFERENCES `class`(`Class_no`)
);

DROP TABLE IF EXISTS `class_and_instructors`;
CREATE TABLE `class_and_instructors`
(
    `Employee_id`    int    NOT NULL,
    `Class_no`    int    NOT NULL,
    PRIMARY KEY(`Employee_id`, `Class_no`),
    FOREIGN KEY(`Employee_id`  ) REFERENCES `employee`(`Employee_id`),
    FOREIGN KEY(`Class_no`  ) REFERENCES `class`(`Class_no`)
);

DROP TABLE IF EXISTS `gym_facility_booking`;
CREATE TABLE `gym_facility_booking`
(
    `Facility_id`   int    NOT NULL,
    `Booking_id`    int    NOT NULL,
    PRIMARY KEY(`Facility_id`, `Booking_id`),
    FOREIGN KEY(`Facility_id`  ) REFERENCES `gym_facility`(`Facility_id`),
    FOREIGN KEY(`Booking_id`  ) REFERENCES `booking`(`Booking_id`)
);

DROP TABLE IF EXISTS `workout_plan_activity`;
CREATE TABLE `workout_plan_activity`
(
    `Plan#` int NOT NULL,
    `Activity_name` varchar(50) NOT NULL,
    PRIMARY KEY(`Plan#` , `Activity_name`),
    FOREIGN KEY(`Plan#`  ) REFERENCES `workout_plan`(`Plan_id`)
);

DROP TABLE IF EXISTS `login_phone_numbers`;
CREATE TABLE `login_phone_numbers`
(
    `Email` varchar(50)  NOT NULL,
    `Phone#`    int NOT NULL,
    PRIMARY KEY(`Email`, `Phone#`),
    FOREIGN KEY(`Email`) REFERENCES LOGIN(`Email`)
);

DROP TABLE IF EXISTS `plan_created_for`;
CREATE TABLE `plan_created_for`
(
    `Plan#`        int        NOT NULL,
    `Member_id`    int        NOT NULL,
    `Instructor_employee_id`    int    NOT NULL,
    PRIMARY KEY(`Plan#`, `Member_id`, `Instructor_employee_id`),
    FOREIGN KEY(`Member_id`) REFERENCES `members`(`Member_id`),
    FOREIGN KEY(`Instructor_employee_id`) REFERENCES `instructor`(`Instructor_employee_id`)
);

INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (1, '2024-01-30');
INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (2, '2024-01-30');
INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (3, '2024-01-30');
INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (4, '2024-01-30');
INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (5, '2024-01-30');
INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (6, '2024-01-30');

INSERT INTO `room` (Room_no, Facility_id)
VALUES (1, 1);
INSERT INTO `room` (Room_no, Facility_id)
VALUES (2, 2);
INSERT INTO `room` (Room_no, Facility_id)
VALUES (3, 3);
INSERT INTO `room` (Room_no, Facility_id)
VALUES (4, 4);
INSERT INTO `room` (Room_no, Facility_id)
VALUES (5, 5);
INSERT INTO `room` (Room_no, Facility_id)
VALUES (6, 6);




INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (7, '2023-01-30');
INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (8, '2023-01-30');
INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (9, '2023-01-30');
INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (10, '2023-01-30');
INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (11, '2023-01-30');
INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (12, '2023-01-30');
INSERT INTO `gym_facility` (Facility_id, Maintenance_date)
VALUES (13, '2023-01-30');


INSERT INTO `machine` (Machine_no, Machine_type, Facility_id)
VALUES (1, 'Chest Press', 7);
INSERT INTO `machine` (Machine_no, Machine_type, Facility_id)
VALUES (2, 'Shoulder Press', 8);
INSERT INTO `machine` (Machine_no, Machine_type, Facility_id)
VALUES (3, 'Bench Press', 9);
INSERT INTO `machine` (Machine_no, Machine_type, Facility_id)
VALUES (4, 'Smith Machine', 10);
INSERT INTO `machine` (Machine_no, Machine_type, Facility_id)
VALUES (5, 'Squat Rack', 11);
INSERT INTO `machine` (Machine_no, Machine_type, Facility_id)
VALUES (6, 'Leg Press', 12);
INSERT INTO `machine` (Machine_no, Machine_type, Facility_id)
VALUES (7, 'Rowing Machine', 13);





-- LOCK TABLES `users` WRITE;

-- UNLOCK TABLES;