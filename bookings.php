<?php
// Initialize the session
session_start();
require_once "config.php";

 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;

}

// Initialize variables

$name = $start = $expiry = "";
$status = False;

$email = $_SESSION["email"];

$sql = "SELECT Name, Member_start_date, Member_expiry_date, Membership_is_active FROM `members` INNER JOIN `login` ON members.Email = login.Email WHERE login.Email = '$email' ";

$result = mysqli_query($link, $sql);

if (mysqli_num_rows($result) > 0) {
  // output data of each row
  while($row = mysqli_fetch_assoc($result)) {

    // set variables for use in HTML
    $name = $row["Name"];
    $start = $row["Member_start_date"];
    $expiry = $row["Member_expiry_date"];

    if($row["Membership_is_active"] == 1) {
        $status = True;
    } else {
        $status = False;
    }
  }

//   echo $name ." " . $start . " " .  $expiry . " " .  $status;


} else {
  echo "0 results";
}

mysqli_close($link);



?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <h2>Hi, <?php echo htmlspecialchars($name); ?>, Welcome to the bookings portal</h2>
    
    <div class="container">
        <div class="row">
            <div class="col-lg-6 mb-4">
                <div class="card">
                    <img class="card-img-top" src="" alt="">
                    <div class="card-body">
                        <h5 class="card-title">Current Bookings</h5>
                        <p class="card-text">
                            View your current bookings!
                        </p>
  
                        <a href="viewMemberBookings.php" class="btn btn-outline-primary btn-sm">
                            View now
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-4">
                <div class="card">
                    <img class="card-img-top" src="" alt="">
  
                    <div class="card-body">
                        <h5 class="card-title">Make a booking</h5>
                        <p class="card-text">
                            Make a booking
                        </p>
                          
                        <a href="createMemberBooking.php" class="btn btn-outline-primary btn-sm">
                            Book now
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <a href="welcome.php" class="btn btn-secondary">Back</a>
    
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>