<?php
// Initialize the session
session_start();
include 'config.php';
$bookMemberemail = $_GET['bookMemberemail'];

$initialSQL = "SELECT Name, Member_id FROM `members` INNER JOIN `login` ON members.Email = login.Email WHERE login.Email = '$bookMemberemail' ";



// We want to first view the room bookings

$name = $member_id = "";

$result = mysqli_query($link, $initialSQL);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while ($row = mysqli_fetch_assoc($result)) {
        // set variables for use in HTML
        $name = $row["Name"];
        $member_id = $row["Member_id"];
    }
} else {
    echo "0 results";
}



?>


<!-- SELECT Member_id, Duration, Date, Start_time, End_time, Room_no, Machine_type from booking
left outer join gym_facility_booking on booking.Booking_id = gym_facility_booking.Booking_id
left outer join gym_facility on gym_facility_booking.Facility_id = gym_facility.Facility_id
left outer join room on gym_facility.Facility_id = room.Facility_id 
left outer join machine on gym_facility.Facility_id = machine.Facility_id -->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Bookings</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <style>
    body {
      font: 14px sans-serif;
      text-align: center;
    }
  </style>

</head>

<body>
<h2>You are seeing bookings of <?php echo htmlspecialchars($name); ?></h2>
  <br>
  <br>
    
  <div class="container">
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Facility Booked</th>
          <th scope="col">Date</th>
          <th scope="col">Start Time</th>
          <th scope="col">End Time</th>
          <th scope="col">Duration</th>
          <th scope="col">Type of Booking</th>
    
        </tr>
      </thead>
      <tbody>
        <?php




        $sql = "SELECT Member_id, Duration, Date, Start_time, End_time, Room_no, Machine_type FROM `booking`
        LEFT OUTER JOIN `gym_facility_booking` on `booking`.Booking_id = `gym_facility_booking`.Booking_id 
        LEFT OUTER JOIN `gym_facility` on `gym_facility_booking`.Facility_id = `gym_facility`.Facility_id 
        LEFT OUTER JOIN `room` on `gym_facility`.Facility_id = `room`.Facility_id 
        LEFT OUTER JOIN `machine` on `gym_facility`.Facility_id = `machine`.Facility_id 
        WHERE Member_id = $member_id";
        $result = mysqli_query($link, $sql);
        if ($result) {

          $facilityname = $bookingtype =  "";
          while ($row = mysqli_fetch_assoc($result)) {
            // Calculate name and type

            if(is_null($row['Machine_type']) && !is_null($row['Room_no'])) {
              $facilityname = "Room #" . $row['Room_no'];
              $bookingtype = "Room Booking";

            } else if(is_null($row['Room_no']) && !is_null($row['Machine_type'])) {
              $facilityname = $row['Machine_type'];
              $bookingtype = "Machine booking";
            }


            $duration = $row['Duration'] . " minutes";
            $date = $row['Date'];
            $endtime = $row['End_time'];
            $starttime = $row['Start_time'];

            echo ' <tr>
        <th scope="row">' . $facilityname . '</th>
        <td>' . $date . '</td>
        <td>' . $starttime . '</td>
        <td>' . $endtime . '</td>
        <td>' . $duration . '</td>
        <td>' . $bookingtype . '</td>
       </td>
 
      </tr>';
          }
        }

        ?>

      </tbody>
    </table>
  </div>

  <a href="view_mem_to_book_by_emp.php" class="btn btn-secondary">Back</a>

</body>

</html>