<?php
// Initialize the session
session_start();
require_once "config.php";

 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}

$name = " ";
$email = $_SESSION["email"];
$sql = "SELECT Name FROM `login` WHERE login.Email = '$email' ";
$result = mysqli_query($link, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
  
      // set variables for use in HTML
      $name = $row["Name"];
    }
  
  } else {
    echo "0 results";
  }
  
  mysqli_close($link);

?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
<h1 class="my-5">Hi, <b><?php echo htmlspecialchars($name); ?></b>. Welcome to our site.</h1>
    <p>
        <a href="info_emp.php" class="btn btn-primary">Employee Information</a>
        <a href="display_mem_info.php" class="btn btn-primary">View and Delete Members</a>
        <a href="update_mem.php" class="btn btn-primary">Update Member Information</a>
        <a href="signup_members_by_emp.php" class="btn btn-primary">Sign up Members</a>
        <a href="view_mem_to_book_by_emp.php" class="btn btn-primary">Book Facilities for Members</a>

        <!-- <a href="reset_emp_password.php" class="btn btn-warning">Reset Your Password</a> -->
        <a href="logout.php" class="btn btn-danger ml-3">Sign Out of Your Account</a>


    </p>
</body>
</html>