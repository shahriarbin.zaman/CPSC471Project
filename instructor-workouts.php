<?php
// Initialize the session
session_start();
require_once "config.php";

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}

$instructor_id = " ";
$email = $_SESSION["email"];
$sql = "SELECT Employee_id FROM `employee` WHERE employee.Email = '$email' ";
$result = mysqli_query($link, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
  
      // set variables for use in HTML
      $instructor_id = $row["Employee_id"];
    }
  
  } else {
    echo "0 results";
  }

//====================================================================================================
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Bookings</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <style>
    body {
      font: 14px sans-serif;
      text-align: center;
    }
  </style>

</head>

<body>
  <h2>Your Workout Plans</h2>
  <br>
  <br>
    
  <div class="container">
    <table class="table">
      <thead>
        <tr>
            <th scope="col">Employee ID</th>
            <th scope="col">Member ID</th>
            <th scope="col">Plan number</th>
            <th scope="col">Duration</th>
            <th scope="col">Date</th>
            <th scope="col">Activity Name</th>
    
        </tr>
      </thead>
      <tbody>
        <?php


        $sql = "SELECT `Plan#`, Activity_name, Member_id, Instructor_employee_id, Duration, Date FROM `plan_created_for`
        NATURAL JOIN `workout_plan_activity` JOIN `workout_plan` ON `workout_plan`.Plan_id = `Plan#`
        WHERE Instructor_employee_id = '$instructor_id'
        ORDER BY Member_id";
        $result = mysqli_query($link, $sql);
        if ($result) {

          while ($row = mysqli_fetch_assoc($result)) {
            // Calculate name and type
            $Plan = $row['Plan#'];
            $Activity_name = $row['Activity_name'];
            $Member_id = $row['Member_id'];
            $Instructor_employee_id = $row['Instructor_employee_id'];
            $duration = $row['Duration'];
            $date = $row['Date'];
            echo ' <tr>
            <th scope="row">' . $Instructor_employee_id . '</th>
            <td>' . $Member_id . '</td>
            <td>' . $Plan . '</td>
            <td>' . $duration . '</td>
            <td>' . $date . '</td>
            <td>' . $Activity_name . '</td>

            </td>
            </tr>';
          }
        }

        ?>

      </tbody>
    </table>
  </div>

  <a href="instructor-page.php" class="btn btn-secondary">Back</a>

</body>

</html>